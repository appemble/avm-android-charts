/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.DoughnutChart;
import org.achartengine.model.MultipleCategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.database.Cursor;

public class DoughnutChartHelper extends ChartHelper {
  List<double[]> values = null;
  List<String[]> titles = null;
  String[] categories = null;
  DefaultRenderer renderer;
  LinkedHashMap<String, Integer> mapCategoryAndCount;
  int[] iColors = null;
  
  DoughnutChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    super(cursor, sType, mAttributes);
    values = new ArrayList<double[]>();
    titles = new ArrayList<String[]>();
  }

  // This function requires cursor to contain following columns 
  // Line Chart and CubicLineChart as x value, y1 value, y2 value, y3 value... 
  // where n(y1, y2, ...) depends upon number of titles specified in the chart definition.
  // For Bar Chart and StackedBarChart, the values are y1, y2, y3, ....
  public static AbstractChart createChart(Cursor cursor, Object renderer, String sType, 
      HashMap<String, String> mAttributes, float fParentHeight) {    
    if (null == cursor || null == renderer || null == sType)
      return null;
    if (!(renderer instanceof DefaultRenderer))
      return null;
    DoughnutChartHelper chartHelper = new DoughnutChartHelper(cursor, sType, mAttributes);
    chartHelper.renderer = (DefaultRenderer)renderer;

    if (false == chartHelper.readDataset())
      return null;
    return chartHelper.createChart();
  }

  // This function assumes that the cursor contains 3 columns. category, title, value. 
  // The rows are ordered by category, title.  
  private boolean readDataset() {
    int iColumnCount = cursor.getColumnCount();
    if (iColumnCount != 3)
      return false;
    int iTitleIndex = cursor.getColumnIndex("title");
    int iCategoryIndex = cursor.getColumnIndex("category");
    int iValueIndex = cursor.getColumnIndex("value");
    iColors = getColors(mAttributes);
    mapCategoryAndCount = getColumnValuesAndCount("category", cursor);
    if (-1 == iTitleIndex || -1 == iCategoryIndex || -1 == iValueIndex || null == iColors || 
        null == mapCategoryAndCount) {
      System.out.println("Local data source must contain column category, title and value.");
      return false;
    }
    int iRow = 0; String sCurrentCategory = null; int iCurrentCategory = -1;
    categories = new String[mapCategoryAndCount.size()];
    for (String sCategory : mapCategoryAndCount.keySet()) {
      titles.add(new String[mapCategoryAndCount.get(sCategory).intValue()]);
      values.add(new double[mapCategoryAndCount.get(sCategory).intValue()]);
    }
    cursor.moveToFirst();
    do {
      String sCur = cursor.getString(iCategoryIndex);
      if (null == sCurrentCategory || (sCur != null && !sCurrentCategory.equalsIgnoreCase(sCur))) {
        sCurrentCategory = sCur; iRow = 0;
        categories[++iCurrentCategory] = sCur;
      }
      titles.get(iCurrentCategory)[iRow] = cursor.getString(iTitleIndex);
      values.get(iCurrentCategory)[iRow] = cursor.getDouble(iValueIndex);
      iRow++;
    } while (cursor.moveToNext());
    
    if (values.get(0).length != iColors.length) {
      System.out.println("Number of colors must be same as number of values in a category");
      return false;
    }
    return true;
  }

  protected AbstractChart createChart() {    
    for (int color : iColors) {
      SimpleSeriesRenderer r = new SimpleSeriesRenderer();
      r.setColor(color);
      renderer.addSeriesRenderer(r);
    }

    MultipleCategorySeries series = new MultipleCategorySeries("Doughnut Chart");
    int k = 0;
    for (double[] value : values) {
      series.add(categories[k], titles.get(k), value);
      k++;
    }
    return new DoughnutChart(series, renderer);
  }
}
