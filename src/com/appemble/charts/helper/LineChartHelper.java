/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.CubicLineChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.ScatterChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.appemble.avm.Utilities;

import android.database.Cursor;

public class LineChartHelper extends ChartHelper {
  List<double[]> xValues = null;
  List<double[]> yValues = null;
  XYMultipleSeriesRenderer renderer;
  
  LineChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    super(cursor, sType, mAttributes);
    xValues = new ArrayList<double[]>();
    yValues = new ArrayList<double[]>();    
  }
  
  // This function assumes that the cursor contains 3 columns. title, x value, y value. The rows are ordered by title.  
  private boolean readTitlesAndDataset() {
    mapTitlesAndCount = getColumnValuesAndCount("title", cursor);
    if (null == mapTitlesAndCount)
      return false;
    Set<String> sTitles = mapTitlesAndCount.keySet();
    for (String sTitle : sTitles) {
      xValues.add(new double[mapTitlesAndCount.get(sTitle).intValue()]);
      yValues.add(new double[mapTitlesAndCount.get(sTitle).intValue()]);
    }
    int iRow = 0; 
    String sCurrentTitle = null; int iCurrentIndex = 0;
    int iTitleIndex = cursor.getColumnIndex("title");
    int iXIndex = cursor.getColumnIndex("x");
    int iYIndex = cursor.getColumnIndex("y");
    if (-1 == iTitleIndex || -1 == iXIndex || -1 == iYIndex) {
      System.out.println("Local data source must contain column title, x and y.");
      return false;
    }
    cursor.moveToFirst();
    do {
      String sTitleDb = cursor.getString(0);
      if (null == sCurrentTitle)
        sCurrentTitle = sTitleDb;
      else if (!(sCurrentTitle.equalsIgnoreCase(sTitleDb))) {
        iCurrentIndex = 0;
        for (String sTitle : sTitles) {
          if (sTitleDb.equals(sTitle)) {
            sCurrentTitle = sTitleDb;
            iRow = 0;
            break;
          }
          iCurrentIndex++;
        }
      }
      xValues.get(iCurrentIndex)[iRow] = cursor.getDouble(1);
      yValues.get(iCurrentIndex)[iRow] = cursor.getDouble(2);
      
      iRow++;
    } while (cursor.moveToNext());
    
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
    
    return true;
  }

  // This function requires cursor to contain following columns 
  // Line Chart and CubicLineChart as x value, y1 value, y2 value, y3 value... 
  // where n(y1, y2, ...) depends upon number of titles specified in the chart definition.
  // For Bar Chart and StackedBarChart, the values are y1, y2, y3, ....
  public static AbstractChart createChart(Cursor cursor, Object renderer, String sType, 
      HashMap<String, String> mAttributes, float fParentHeight) {
    if (null == cursor || null == renderer || null == sType)
      return null;
    if (!(renderer instanceof XYMultipleSeriesRenderer))
      return null;
    LineChartHelper chartHelper = new LineChartHelper(cursor, sType, mAttributes);
    chartHelper.renderer = (XYMultipleSeriesRenderer)renderer;

    if (false == chartHelper.readDataset())
      return null;
    return chartHelper.createChart(fParentHeight);
  }
  
  private boolean readDataset() {    
    String[] aTitles = ChartHelper.getTitles(mAttributes);
    if (null == aTitles)
      return readTitlesAndDataset();
    // The first column represents X. Rest of the columns represent 1 or more y values.
    int i, iColumnCount = cursor.getColumnCount(), iCount = cursor.getCount();
    if (iColumnCount - 1 != aTitles.length) // In other charts, the first column is x, and rest of the column are y0, y1, ..yn
      return false;
    mapTitlesAndCount = new LinkedHashMap<String, Integer>();
    for (i = 0; null != aTitles && i < aTitles.length; i++) {
      xValues.add(new double[iCount]);
      yValues.add(new double[iCount]);
      mapTitlesAndCount.put(aTitles[i], iCount);
    }
    int iRow = 0;
    cursor.moveToFirst();
    do {
        for (i = 0; null != aTitles && i < aTitles.length; i++)
          xValues.get(i)[iRow] = cursor.getDouble(0);
        for (i = 0; null != aTitles && i < aTitles.length; i++) {
          yValues.get(i)[iRow] = cursor.getDouble(i + 1);
      }
      iRow++;
    } while (cursor.moveToNext());
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
    
    return true;
  }
  
  protected AbstractChart createChart(float fParentHeight) {
    if (null == mapTitlesAndCount || 0 == xValues.size() || 0 == yValues.size())
      return null;
    int i = 0; Set<String> sTitles = mapTitlesAndCount.keySet();
    AbstractChart chart = null;
    int[] aColors = getColors(mAttributes);
    PointStyle[] aPointStyles = getPointStyles(mAttributes);      
    String s = mAttributes.get("line_width");
    float fLineWidth = 1;
    if (null != s && s.length() > 0)
      fLineWidth = (int)Utilities.getDimension(s.trim(), fParentHeight);
    s = mAttributes.get("point_stroke_width");
    int iStrokeWidth = 1;
    if (null != s && s.length() > 0)
      iStrokeWidth = (int)Utilities.getDimension(s.trim(), fParentHeight);
    s = mAttributes.get("fill_points");
    boolean bFillPoints = false;
    if (null != s && s.length() > 0)
      bFillPoints = Utilities.parseBoolean(s.trim());
    s = mAttributes.get("stroke");
    BasicStroke stroke = BasicStroke.SOLID;
    if (s != null && s.length() > 0) {
      if (s.equalsIgnoreCase("DASHED"))
        stroke = BasicStroke.DASHED;
      else if (s.equalsIgnoreCase("DOTTED"))
        stroke = BasicStroke.DOTTED;
    }
    BasicStroke[] aStrokes = getStrokes(mAttributes);
    for (i = 0; i < aColors.length; i++) {
      XYSeriesRenderer r = new XYSeriesRenderer();
      r.setColor(aColors[i]);
      r.setFillPoints(bFillPoints);
      r.setPointStyle(aPointStyles[i]);
      r.setPointStrokeWidth(iStrokeWidth);
      r.setStroke(null != aStrokes && aStrokes.length > i ? aStrokes[i] : stroke);
      r.setLineWidth(fLineWidth);
      renderer.addSeriesRenderer(r);
    }
    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    i = 0;
    for (String sTitle : sTitles) {
      // XYSeries series = new XYSeries(aTitles[i], scale);
      XYSeries series = new XYSeries(sTitle, 0);
      double[] xV = xValues.get(i);
      double[] yV = yValues.get(i);
      int seriesLength = xV.length;
      for (int k = 0; k < seriesLength; k++)
        series.add(xV[k], yV[k]);
      dataset.addSeries(series);
      i++;
    }
    
    if (sType.equalsIgnoreCase("LineChart"))
      chart = new LineChart(dataset, renderer);
    else if (sType.equalsIgnoreCase("CubicLineChart"))
      chart = new CubicLineChart(dataset, renderer, .33f);
    else if (sType.equalsIgnoreCase("ScatterChart")) {
      chart = new ScatterChart(dataset, renderer);
    }
    return chart;
  } 
}
