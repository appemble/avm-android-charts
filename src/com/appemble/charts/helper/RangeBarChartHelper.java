/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.chart.RangeBarChart;
import org.achartengine.model.RangeCategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.database.Cursor;
import android.graphics.Color;

public class RangeBarChartHelper extends ChartHelper {
  List<double[]> xValues = null;
  List<double[]> yValues = null;
  XYMultipleSeriesRenderer mRenderer;
  
  RangeBarChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    super(cursor, sType, mAttributes);
    xValues = new ArrayList<double[]>();
    yValues = new ArrayList<double[]>();    
  }

  // This function requires cursor to contain following columns 
  // Line Chart and CubicLineChart as x value, y1 value, y2 value, y3 value... 
  // where n(y1, y2, ...) depends upon number of titles specified in the chart definition.
  // For Bar Chart and StackedBarChart, the values are y1, y2, y3, ....
  public static AbstractChart createChart(Cursor cursor, Object mRenderer, String sType, 
      HashMap<String, String> mAttributes, float fParentHeight) {    
    if (null == cursor || null == mRenderer || null == sType)
      return null;
    if (!(mRenderer instanceof XYMultipleSeriesRenderer))
      return null;
    RangeBarChartHelper chartHelper = new RangeBarChartHelper(cursor, sType, mAttributes);
    chartHelper.mRenderer = (XYMultipleSeriesRenderer)mRenderer;

    if (false == chartHelper.readDataset())
      return null;
    return chartHelper.createChart();
  }

  // This function requires cursor to contain following columns 
  // RangeBar Chart min value, max value 
  private boolean readDataset() {
    String[] aTitles = getTitles(mAttributes);
    if (null == aTitles)
      return false; // title must be present in the definition file
    if (cursor.getColumnCount() != 2) // In a range bar chart, the first column is min value, 2nd column is max value
      return false;
    int iCount = cursor.getCount();
    xValues.add(new double[iCount]);
    yValues.add(new double[iCount]);
    int iRow = 0;
    int iMinIndex = cursor.getColumnIndex("min");
    int iMaxIndex = cursor.getColumnIndex("max");
    cursor.moveToFirst();
    do {
      xValues.get(0)[iRow] = cursor.getDouble(iMinIndex);
      yValues.get(0)[iRow] = cursor.getDouble(iMaxIndex);
      iRow++;
    } while (cursor.moveToNext());
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
    
    return true;
  }

  private AbstractChart createChart() {    
    int i = 0; String[] sTitles = getTitles(mAttributes);
    if (null == sTitles) {
      System.out.println("Titles must be present in the chart definition XML file.");
      return null;
    }
    int[] aColors = getColors(mAttributes);
    String s;
    XYSeriesRenderer r = new XYSeriesRenderer();
    r.setColor(aColors[i]);
/* commented with V1.1 of AChartEngine    
    s = mAttributes.get("displayChartValues");
    if (null != s && s.length() > 0)
      r.setDisplayChartValues(Boolean.parseBoolean(s.trim()));
    s = mAttributes.get("chartValuesTextSize");
    if (null != s && s.length() > 0)
      r.setChartValuesTextSize(Integer.parseInt(s.trim()));
    s = mAttributes.get("displayChartValuesSpacing");
    if (null != s && s.length() > 0)
      r.setChartValuesSpacing(Integer.parseInt(s.trim()));
*/    
    s = mAttributes.get("gradientEnabled");
    if (null != s && s.length() > 0)
      r.setGradientEnabled(Boolean.parseBoolean(s.trim()));
    Double dStart = 0.0; int color = 0;
    s = mAttributes.get("gradientStartValue");
    if (null != s && s.length() > 0)
      dStart = Double.parseDouble(s.trim());
    s = mAttributes.get("gradientStartColor");
    if (null != s && s.length() > 0)
      color = Color.parseColor(s.trim());
    if (dStart != 0.0 && color != 0)
      r.setGradientStart(dStart, color);
    dStart = 0.0; color = 0;
    s = mAttributes.get("gradientStopValue");
    if (null != s && s.length() > 0)
      dStart = Double.parseDouble(s.trim());
    s = mAttributes.get("gradientStopColor");
    if (null != s && s.length() > 0)
      color = Color.parseColor(s.trim());
    if (dStart != 0.0 && color != 0)
      r.setGradientStop(dStart, color);
    mRenderer.addSeriesRenderer(r);
    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    RangeCategorySeries series = new RangeCategorySeries(sTitles[0]);
    double[] x = xValues.get(0);
    double[] y = yValues.get(0);
    int seriesLength = y.length;
    for (int k = 0; k < seriesLength; k++) {
      series.add(java.lang.Math.min(x[k], y[k]), java.lang.Math.max(x[k], y[k]));
    }
    dataset.addSeries(series.toXYSeries());
    return new RangeBarChart(dataset, mRenderer, Type.DEFAULT);
  } 

  
}
