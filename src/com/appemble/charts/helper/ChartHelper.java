/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.achartengine.chart.PointStyle;
import org.achartengine.renderer.BasicStroke;

import android.database.Cursor;
import android.graphics.Color;

import com.appemble.avm.Utilities;

public class ChartHelper {
  LinkedHashMap<String, Integer> mapTitlesAndCount = null;
  HashMap<String, String> mAttributes = null;
  Cursor cursor = null;
  String sType = null;
  
  protected ChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    this.cursor = cursor;
    this.sType = sType;
    this.mAttributes = mAttributes;    
  }

  protected static LinkedHashMap<String, Integer> getColumnValuesAndCount(String sColumnName, Cursor cursor) {
    if (null == cursor || cursor.isClosed() || false == cursor.moveToFirst())
      return null;
    int iColumnIndex = cursor.getColumnIndex(sColumnName);
    if (iColumnIndex < 0)
      return null;
    LinkedHashMap <String, Integer> mapTitlesAndCount = new LinkedHashMap<String, Integer>();
    String sTitle; Integer iCount;
    do {
      sTitle = cursor.getString(iColumnIndex);
      if (null == sTitle)
        continue;
      iCount = mapTitlesAndCount.get(sTitle);
      if (iCount != null)
        mapTitlesAndCount.put(sTitle, Integer.valueOf(iCount.intValue()+1));
      else
        mapTitlesAndCount.put(sTitle, Integer.valueOf(1));
    } while (cursor.moveToNext());
    cursor.moveToFirst();
    return mapTitlesAndCount; 
  }





  protected static String[] getTitles(HashMap<String, String> mAttributes) {
    String sTitles = mAttributes.get("titles");
    if (null != sTitles && sTitles.length() > 0)
      return Utilities.splitCommaSeparatedString(sTitles);
    return null;
  }
  
  protected static int[] getColors(HashMap<String, String> mAttributes) {
    String sColors = mAttributes.get("colors");
    int[] aColors = null;
    if (null != sColors && sColors.length() > 0) {
      String[] vColors = Utilities.splitCommaSeparatedString(sColors);
      aColors = new int[vColors.length];
      for (int i = 0; i < vColors.length; i++)
        aColors[i] = Color.parseColor(vColors[i]);
    }
    return aColors;
  }
  
  protected static BasicStroke[] getStrokes(HashMap<String, String> mAttributes) {
    String sStrokes = mAttributes.get("strokes");
    if (null == sStrokes || sStrokes.length() == 0)
      return null;
    String[] vStrokes = Utilities.splitCommaSeparatedString(sStrokes);
    BasicStroke[] aStrokes = new BasicStroke[vStrokes.length];
    BasicStroke stroke = null;
    for (int i = 0; i < vStrokes.length; i++) {
      stroke = BasicStroke.SOLID;
      if (vStrokes[i] != null && vStrokes[i].length() > 0) {
        if (vStrokes[i].trim().equalsIgnoreCase("DASHED"))
          stroke = BasicStroke.DASHED;
        else if (vStrokes[i].trim().equalsIgnoreCase("DOTTED"))
          stroke = BasicStroke.DOTTED;
      }
      aStrokes[i] = stroke;
    }
    return aStrokes;
  }
  
  protected static PointStyle[] getPointStyles(HashMap<String, String> mAttributes) {
    String sPointStyles = mAttributes.get("point_styles");
    PointStyle[] aPointStyles = null;
    if (null != sPointStyles && sPointStyles.length() > 0) {
      String[] vPointStyles = Utilities.splitCommaSeparatedString(sPointStyles);
      aPointStyles = new PointStyle[vPointStyles.length];
      for (int i = 0; i < vPointStyles.length; i++) {
        String sPointStyle = vPointStyles[i].trim(); 
        if (sPointStyle.equalsIgnoreCase("CIRCLE"))
          aPointStyles[i] = PointStyle.CIRCLE;
        else if (sPointStyle.equalsIgnoreCase("TRIANGLE"))
          aPointStyles[i] = PointStyle.TRIANGLE;
        else if (sPointStyle.equalsIgnoreCase("DIAMOND"))
          aPointStyles[i] = PointStyle.DIAMOND;
        else if (sPointStyle.equalsIgnoreCase("SQUARE"))
          aPointStyles[i] = PointStyle.SQUARE;
        else if (sPointStyle.equalsIgnoreCase("POINT"))
          aPointStyles[i] = PointStyle.POINT;
        else if (sPointStyle.equalsIgnoreCase("X"))
          aPointStyles[i] = PointStyle.X;
        else
          System.out.println("Point Style must be CIRCLE, TRIANGLE, DIAMOND, SQUARE, POINT or X");
      }
    }
    return aPointStyles;
  }
//  if (sType.equalsIgnoreCase("TimeChart")) 
//      sTimeChartDateFormat = mAttributes.get("dateFormat");
  
}
