/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.TimeChart;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.database.Cursor;

import com.appemble.avm.Cache;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;

public class TimeChartHelper extends ChartHelper {
  List<Date[]> xValues;
  List<double[]> yValues;
  XYMultipleSeriesRenderer mRenderer;
  
  TimeChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    super(cursor, sType, mAttributes);
    xValues = new ArrayList<Date[]>();
    yValues = new ArrayList<double[]>();    
  }
  
  public static AbstractChart createChart(Cursor cursor, Object mRenderer, String sType, 
      HashMap<String, String> mAttributes, float fParentHeight) {    
    if (null == cursor || null == mRenderer || null == sType)
      return null;
    if (false == sType.equalsIgnoreCase("TimeChart"))
      return null;
    if (!(mRenderer instanceof XYMultipleSeriesRenderer))
      return null;
    TimeChartHelper chartHelper = new TimeChartHelper(cursor, sType, mAttributes);
    chartHelper.mRenderer = (XYMultipleSeriesRenderer)mRenderer;

    if (false == chartHelper.readDataset())
      return null;
    return chartHelper.createChart(fParentHeight);
  }

  // This function requires cursor to contain following columns 
  // Time Chart x value represents time, y1 value, y2 value, y3 value... 
  // where y1, y2 depends upon number of titles specified in the chart definition.
  protected boolean readDataset() {
    String[] aTitles = getTitles(mAttributes);
    if (null == aTitles)
      return readTitlesAndDataset();
    if (cursor.getColumnCount()-1 != aTitles.length)
      return false;
    mapTitlesAndCount = new LinkedHashMap<String, Integer>();
    int i, iCount = cursor.getCount();
    for (i = 0; null != aTitles && i < aTitles.length; i++) {
      xValues.add(new Date[iCount]);
      yValues.add(new double[iCount]);
      mapTitlesAndCount.put(aTitles[i], iCount);
    }
    int iRow = 0;
    Date date = null; 
    long lXMax = Long.MIN_VALUE, lXMin = Long.MAX_VALUE;
    double dYMax = Double.MIN_VALUE, dYMin = Double.MAX_VALUE;
    String sTimeChartDateFormat = mAttributes.get("date_storage_format");
    if (null == sTimeChartDateFormat) {
        LogManager.logError("Please specify date_format attribute. It is a mandatory attribute.");
        return false;
    }
      
    SimpleDateFormat sdf = new SimpleDateFormat(sTimeChartDateFormat, Cache.context.getResources().getConfiguration().locale);
    cursor.moveToFirst();
    do {
      String sDate = cursor.getString(0);
      try {
        date = sdf.parse(sDate);
      } catch (ParseException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        continue;
      }
      for (i = 0; null != aTitles && i < aTitles.length; i++) {
        xValues.get(i)[iRow] = date;
        long lTime = date.getTime();
        lXMin = java.lang.Math.min(lXMin, lTime);
        lXMax = java.lang.Math.max(lXMax, lTime);
        yValues.get(i)[iRow] = cursor.getDouble(i+1);
        dYMin = java.lang.Math.min(dYMin, yValues.get(i)[iRow]);
        dYMax = java.lang.Math.max(dYMax, yValues.get(i)[iRow]);
      }
      iRow++;
    } while (cursor.moveToNext());
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
    
    long l = parseTime(mAttributes.get("x_min"), sTimeChartDateFormat);
    if (0 == l)
      l = parseTime(mAttributes.get("x_min"), lXMin);
    if (l != 0)
      ((XYMultipleSeriesRenderer)mRenderer).setXAxisMin(l);
    l = parseTime(mAttributes.get("x_max"), sTimeChartDateFormat);
    if (0 == l)
      l = parseTime(mAttributes.get("x_max"), lXMax);
    if (l != 0)
      ((XYMultipleSeriesRenderer) mRenderer).setXAxisMax(l);
    
    
    String s =  mAttributes.get("y_min");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMin += dYMin*Double.parseDouble(s.trim())/100;
      ((XYMultipleSeriesRenderer)mRenderer).setYAxisMin(dYMin);
    }
    s =  mAttributes.get("y_max");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMax += dYMax*Double.parseDouble(s.trim())/100;
      ((XYMultipleSeriesRenderer)mRenderer).setYAxisMax(dYMax);
    }

    return true;
  }


  // This function assumes that the cursor contains 3 columns. title, x value (timeline), y value. x Value contains dates. 
  // The rows are ordered by title.  
  protected boolean readTitlesAndDataset() {
    mapTitlesAndCount = getColumnValuesAndCount("title", cursor);
    if (null == mapTitlesAndCount)
      return false;
    Set<String> sTitles = mapTitlesAndCount.keySet();
    for (String sTitle : sTitles) {
      xValues.add(new Date[mapTitlesAndCount.get(sTitle).intValue()]);
      yValues.add(new double[mapTitlesAndCount.get(sTitle).intValue()]);
    }
    int iRow = 0; 
    String sCurrentTitle = null; int iCurrentIndex = 0;
    String sTimeChartDateFormat = mAttributes.get("date_storage_format");
    SimpleDateFormat sdf = new SimpleDateFormat(sTimeChartDateFormat, Cache.context.getResources().getConfiguration().locale);
    long lXMax = Long.MIN_VALUE, lXMin = Long.MAX_VALUE;
    double dYMax = Double.MIN_VALUE, dYMin = Double.MAX_VALUE;
    cursor.moveToFirst();
    do {
      String sTitleDb = cursor.getString(0);
      if (null == sCurrentTitle)
        sCurrentTitle = sTitleDb;
      else if (!(sCurrentTitle.equalsIgnoreCase(sTitleDb))) {
        iCurrentIndex = 0;
        for (String sTitle : sTitles) {
          if (sTitleDb.equals(sTitle)) {
            iRow = 0;
            break;
          }
          iCurrentIndex++;
        }
      }
      String sDate = cursor.getString(1);
      Date date = null;
      try {
        date = sdf.parse(sDate);
      } catch (ParseException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        continue;
      }
      xValues.get(iCurrentIndex)[iRow] = date;
      long lTime = date.getTime();
      lXMin = java.lang.Math.min(lXMin, lTime);
      lXMax = java.lang.Math.max(lXMax, lTime);
      yValues.get(iCurrentIndex)[iRow] = cursor.getDouble(2);
      dYMin = java.lang.Math.min(dYMin, yValues.get(iCurrentIndex)[iRow]);
      dYMax = java.lang.Math.max(dYMax, yValues.get(iCurrentIndex)[iRow]);
      
      iRow++;
    } while (cursor.moveToNext());
    
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
    long l = parseTime(mAttributes.get("x_min"), lXMin);
    if (l != 0)
      mRenderer.setXAxisMin(lXMin);
    l = parseTime(mAttributes.get("x_max"), lXMax);
    if (l != 0)
      mRenderer.setXAxisMax(lXMax);
        
    String s =  mAttributes.get("y_min");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMin += dYMin*Double.parseDouble(s.trim())/100;
      mRenderer.setYAxisMax(dYMax);
    }
    s =  mAttributes.get("y_max");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMax += dYMax*Double.parseDouble(s.trim())/100;
      mRenderer.setYAxisMax(dYMax);
    }
    return true;
  }

  protected AbstractChart createChart(float fParentHeight) {
    if (0 == xValues.size() || null == mapTitlesAndCount || 0 == mapTitlesAndCount.size())
      return null;
    int[] aColors = getColors(mAttributes);
    PointStyle[] aPointStyles = getPointStyles(mAttributes);
    if (null == aColors || null == aPointStyles)
      return null;
    int i = 0; Set<String> sTitles = mapTitlesAndCount.keySet();
    if (aColors.length != sTitles.size() || aPointStyles.length != sTitles.size()) {
      System.out.println("The number of colors or points does not equal to number of titles.");
      return null;
    }
    String s = mAttributes.get("line_width");
    float fLineWidth = 1;
    if (null != s && s.length() > 0)
      fLineWidth = (int)Utilities.getDimension(s.trim(), fParentHeight);
    s = mAttributes.get("point_stroke_width");
    int iStrokeWidth = 1;
    if (null != s && s.length() > 0)
      iStrokeWidth = (int)Utilities.getDimension(s.trim(), fParentHeight);
    boolean bFillPoints = false;
    if (null != s && s.length() > 0)
      bFillPoints = Utilities.parseBoolean(s.trim());
    BasicStroke stroke = BasicStroke.SOLID;
    if (s != null && s.length() > 0) {
      if (s.equalsIgnoreCase("DASHED"))
        stroke = BasicStroke.DASHED;
      else if (s.equalsIgnoreCase("DOTTED"))
        stroke = BasicStroke.DOTTED;
    }
    BasicStroke[] aStrokes = getStrokes(mAttributes);
    for (i = 0; i < aColors.length; i++) {
      XYSeriesRenderer r = new XYSeriesRenderer();
      r.setColor(aColors[i]);
      r.setFillPoints(bFillPoints);
      r.setPointStyle(aPointStyles[i]);
      r.setPointStrokeWidth(iStrokeWidth);
      r.setStroke(null != aStrokes && aStrokes.length > i ? aStrokes[i] : stroke);
      r.setLineWidth(fLineWidth);
      mRenderer.addSeriesRenderer(r);
    }
    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    i = 0;
    for (String sTitle : sTitles) {
      TimeSeries series = new TimeSeries(sTitle);
      Date[] xV = xValues.get(i);
      double[] yV = yValues.get(i);
      int seriesLength = xV.length;
      for (int k = 0; k < seriesLength; k++)
        series.add(xV[k], yV[k]);
      dataset.addSeries(series);
      i++;
    }
      
    TimeChart timeChart = new TimeChart(dataset, mRenderer);
    String sTimeChartDisplayFormat = mAttributes.get("date_display_format");
    if (null != sTimeChartDisplayFormat)
      timeChart.setDateFormat(sTimeChartDisplayFormat);
    return timeChart;
  }  
  
  long parseTime(String sTime, String sFormat) {
      if (null == sTime || sTime.length() == 0 || null == sFormat)
          return 0;
      SimpleDateFormat sdf = new SimpleDateFormat(sFormat);
      try {
        Date d = sdf.parse(sTime);
        if (null != d)
          return d.getTime();
      } catch (ParseException pe) {
//        LogManager.logError(pe, "Unable to parse date");
      }
      return 0;
  }
  
  long parseTime(String sTime, long lTime) {
    if (null == sTime || sTime.length() == 0)
      return 0;
    if (sTime.equalsIgnoreCase("now"))
      return System.currentTimeMillis();
    else if (sTime.indexOf('%') != -1 ) {
      sTime = sTime.substring(0, sTime.indexOf('%'));
      lTime += (System.currentTimeMillis() - lTime)*Double.parseDouble(sTime.trim())/100.0;
      return lTime;
    }
    StringTokenizer stringTokenizer = new StringTokenizer(sTime);
    double length = 0;
    if (stringTokenizer.hasMoreTokens())
      length = Double.parseDouble(stringTokenizer.nextElement().toString());
    String sUnit = null; long lDuration = 0;
    if (stringTokenizer.hasMoreTokens())
      sUnit = stringTokenizer.nextElement().toString();
    if (null == sUnit || sUnit.equals("day"))
      lDuration = 86400*(long)length;
    else if (sUnit.equalsIgnoreCase("min"))
      lDuration = 60*(long)length;
    else if (sUnit.equalsIgnoreCase("hour"))
      lDuration = 3600*(long)length;
    else if (sUnit.equalsIgnoreCase("week"))
      lDuration = 604800*(long)length;
    else if (sUnit.equalsIgnoreCase("month"))
      lDuration = 2592000*(long)length;
    else if (sUnit.equalsIgnoreCase("year"))
      lDuration = 31536000*(long)length;
    
    return System.currentTimeMillis() + lDuration*1000;
  }
}
