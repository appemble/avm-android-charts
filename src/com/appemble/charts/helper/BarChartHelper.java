/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.database.Cursor;

public class BarChartHelper extends ChartHelper {
  List<double[]> yValues = null;
  XYMultipleSeriesRenderer mRenderer;
  
  BarChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    super(cursor, sType, mAttributes);
    yValues = new ArrayList<double[]>();    
  }
  
  // This function requires cursor to contain following columns 
  // For Bar Chart and StackedBarChart, the values are y1, y2, y3, ....
  public static AbstractChart createChart(Cursor cursor, Object mRenderer, String sType, 
      HashMap<String, String> mAttributes, float fParentHeight) {    
    if (null == cursor || null == mRenderer || null == sType)
      return null;
    if (!(mRenderer instanceof XYMultipleSeriesRenderer))
      return null;
    BarChartHelper chartHelper = new BarChartHelper(cursor, sType, mAttributes);
    chartHelper.mRenderer = (XYMultipleSeriesRenderer)mRenderer;

    if (false == chartHelper.readDataset())
      return null;
    return chartHelper.createChart();
  }

  // This function assumes that the cursor contains 2 columns. title, y value. The rows are ordered by title.  
  private boolean readTitlesAndDataset() {
    mapTitlesAndCount = getColumnValuesAndCount("title", cursor);
    if (null == mapTitlesAndCount)
      return false;
    Set<String> sTitles = mapTitlesAndCount.keySet();
    for (String sTitle : sTitles)
      yValues.add(new double[mapTitlesAndCount.get(sTitle).intValue()]);
    int iRow = 0; 
    double dYMax = Double.MIN_VALUE, dYMin = Double.MAX_VALUE;
    String sCurrentTitle = null; int iCurrentIndex = 0;
    int iTitleIndex = cursor.getColumnIndex("title");
    int iYIndex = cursor.getColumnIndex("y");
    if (-1 == iTitleIndex || -1 == iYIndex)
      return false;
    cursor.moveToFirst();
    do {
      String sTitleDb = cursor.getString(iTitleIndex);
      if (null == sCurrentTitle)
        sCurrentTitle = sTitleDb;
      else if (!(sCurrentTitle.equalsIgnoreCase(sTitleDb))) {
        iCurrentIndex = 0;
        for (String sTitle : sTitles) {
          if (sTitleDb.equals(sTitle)) {
            sCurrentTitle = sTitleDb;
            iRow = 0;
            break;
          }
          iCurrentIndex++;
        }
      }
      yValues.get(iCurrentIndex)[iRow] = cursor.getDouble(iYIndex);
      dYMin = java.lang.Math.min(dYMin, yValues.get(iCurrentIndex)[iRow]);
      dYMax = java.lang.Math.max(dYMax, yValues.get(iCurrentIndex)[iRow]);
      
      iRow++;
    } while (cursor.moveToNext());
    
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
        
    String s =  mAttributes.get("y_min");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMin += dYMin*Double.parseDouble(s.trim())/100;
      ((XYMultipleSeriesRenderer)mRenderer).setYAxisMin(dYMin);
    }
    s =  mAttributes.get("y_max");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMax += dYMax*Double.parseDouble(s.trim())/100;
      ((XYMultipleSeriesRenderer)mRenderer).setYAxisMax(dYMax);
    }

    return true;
  }

  private boolean readDataset() {    
    String[] aTitles = ChartHelper.getTitles(mAttributes);
    if (null == aTitles)
      return readTitlesAndDataset();
    // The first column represents X. Rest of the columns represent 1 or more y values.
    int i, iColumnCount = cursor.getColumnCount(), iCount = cursor.getCount();
    if (iColumnCount != aTitles.length) // In other charts, columns are y0, y1, ..yn
        return false;
    mapTitlesAndCount = new LinkedHashMap<String, Integer>();
    for (i = 0; null != aTitles && i < aTitles.length; i++) {
      yValues.add(new double[iCount]);
      mapTitlesAndCount.put(aTitles[i], iCount);
    }
    int iRow = 0;
    double dYMax = Double.MIN_VALUE, dYMin = Double.MAX_VALUE;
    cursor.moveToFirst();
    do {
      for (i = 0; null != aTitles && i < aTitles.length; i++) {
          yValues.get(i)[iRow] = cursor.getDouble(i);
          dYMin = java.lang.Math.min(dYMin, yValues.get(i)[iRow]);
          dYMax = java.lang.Math.max(dYMax, yValues.get(i)[iRow]);
      }
      iRow++;
    } while (cursor.moveToNext());
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
    
    String s =  mAttributes.get("y_min");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMin += dYMin*Double.parseDouble(s.trim())/100;
      ((XYMultipleSeriesRenderer)mRenderer).setYAxisMin(dYMin);
    }
    s =  mAttributes.get("y_max");
    if (s.indexOf('%') != -1 ) {
      s = s.substring(0, s.indexOf('%'));
      dYMax += dYMax*Double.parseDouble(s.trim())/100;
      ((XYMultipleSeriesRenderer)mRenderer).setYAxisMax(dYMax);
    }

    return true;
  }
  
  protected AbstractChart createChart() {
    if (null == mapTitlesAndCount || 0 == yValues.size())
      return null;    
    int i = 0; Set<String> sTitles = mapTitlesAndCount.keySet();
    AbstractChart chart = null;
    int[] aColors = getColors(mAttributes);
    for (i = 0; i < aColors.length; i++) {
      XYSeriesRenderer r = new XYSeriesRenderer();
      r.setColor(aColors[i]);
      mRenderer.addSeriesRenderer(r);
    }
    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    i = 0;
    for (String sTitle : sTitles) {
      CategorySeries series = new CategorySeries(sTitle);
      double[] v = yValues.get(i);
      int seriesLength = v.length;
      for (int k = 0; k < seriesLength; k++)
        series.add(v[k]);
      dataset.addSeries(series.toXYSeries());
      i++;
    }
    if (sType.equalsIgnoreCase("BarChart"))
      chart = new BarChart(dataset, mRenderer, Type.DEFAULT);
    else if (sType.equalsIgnoreCase("StackedBarChart"))
      chart = new BarChart(dataset, mRenderer, Type.STACKED);
    return chart;
  } 
}
