/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.BubbleChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYValueSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.database.Cursor;

public class BubbleChartHelper extends ChartHelper {
  List<double[]> xValues = null;
  List<double[]> yValues = null;
  List<double[]> vValues = null;
  XYMultipleSeriesRenderer renderer;
  
  BubbleChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    super(cursor, sType, mAttributes);
    xValues = new ArrayList<double[]>();
    yValues = new ArrayList<double[]>();    
    vValues = new ArrayList<double[]>();
  }

  // This function requires cursor to contain following columns 
  // Line Chart and CubicLineChart as x value, y1 value, y2 value, y3 value... 
  // where n(y1, y2, ...) depends upon number of titles specified in the chart definition.
  // For Bar Chart and StackedBarChart, the values are y1, y2, y3, ....
  public static AbstractChart createChart(Cursor cursor, Object renderer, String sType, 
      HashMap<String, String> mAttributes, float fParentHeight) {    
    if (null == cursor || null == renderer || null == sType)
      return null;
    if (!(renderer instanceof XYMultipleSeriesRenderer))
      return null;
    BubbleChartHelper chartHelper = new BubbleChartHelper(cursor, sType, mAttributes);
    chartHelper.renderer = (XYMultipleSeriesRenderer)renderer;

    if (false == chartHelper.readDataset())
      return null;
    return chartHelper.createChart();
  }

  // This function requires cursor to contain following columns 
  // title, x , y , value 
  private boolean readDataset() {
    mapTitlesAndCount = getColumnValuesAndCount("title", cursor);
    if (null == mapTitlesAndCount)
      return false;
    Set<String> sTitles = mapTitlesAndCount.keySet();
    for (String sTitle : sTitles) {
      xValues.add(new double[mapTitlesAndCount.get(sTitle).intValue()]);
      yValues.add(new double[mapTitlesAndCount.get(sTitle).intValue()]);
      vValues.add(new double[mapTitlesAndCount.get(sTitle).intValue()]);
    }
    int iRow = 0; 
    String sCurrentTitle = null; int iCurrentIndex = 0;
    int iTitleIndex = cursor.getColumnIndex("title");
    int iXIndex = cursor.getColumnIndex("x");
    int iYIndex = cursor.getColumnIndex("y");
    int iValueIndex = cursor.getColumnIndex("value");
    if (-1 == iTitleIndex || -1 == iYIndex || -1 == iXIndex || -1 == iValueIndex) {
      System.out.println("The local data source query must contain following columns: title, x, y, value");
      return false; 
    }
    cursor.moveToFirst();
    do {
      String sTitleDb = cursor.getString(iTitleIndex);
      if (null == sCurrentTitle)
        sCurrentTitle = sTitleDb;
      else if (!(sCurrentTitle.equalsIgnoreCase(sTitleDb))) {
        iCurrentIndex = 0;
        for (String sTitle : sTitles) {
          if (sTitleDb.equals(sTitle)) {
            iRow = 0;
            sCurrentTitle = sTitleDb;
            break;
          }
          iCurrentIndex++;
        }
      }
      xValues.get(iCurrentIndex)[iRow] = cursor.getDouble(iXIndex);
      yValues.get(iCurrentIndex)[iRow] = cursor.getDouble(iYIndex);
      vValues.get(iCurrentIndex)[iRow] = cursor.getDouble(iValueIndex);
      
      iRow++;
    } while (cursor.moveToNext());
    
    if (null == yValues || yValues.size() == 0 || yValues.get(0).length == 0)
      return false;
    
    return true;
  }
  
  protected AbstractChart createChart() {    
    int i = 0; Set<String> sTitles = mapTitlesAndCount.keySet();
    int[] aColors = getColors(mAttributes);
    if (aColors.length != mapTitlesAndCount.size())
      return null; // colors must be same count as titles
    for (i = 0; i < aColors.length; i++) {
      XYSeriesRenderer r = new XYSeriesRenderer();
      r.setColor(aColors[i]);
      renderer.addSeriesRenderer(r);
    }
    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    i = 0;
    for (String sTitle : sTitles) {
      XYValueSeries series = new XYValueSeries(sTitle);
      double[] xV = xValues.get(i);
      double[] yV = yValues.get(i);
      double[] vV = vValues.get(i);
      int seriesLength = xV.length;
      for (int k = 0; k < seriesLength; k++)
        series.add(xV[k], yV[k], vV[k]);
      dataset.addSeries(series);
      i++;
    }    
    return new BubbleChart(dataset, renderer);
  } 
}
