/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.HashMap;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.DialChart;
import org.achartengine.chart.PieChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.DialRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.database.Cursor;
import android.graphics.Color;

public class PieChartHelper extends ChartHelper {
  String[] sTitles = null;
  double[] values = null;
  int[] iColors = null;
  DefaultRenderer renderer;
  
  PieChartHelper(Cursor cursor, String sType, HashMap<String, String> mAttributes) {
    super(cursor, sType, mAttributes);
  }

  // This function requires cursor to contain following columns 
  // Line Chart and CubicLineChart as x value, y1 value, y2 value, y3 value... 
  // where n(y1, y2, ...) depends upon number of titles specified in the chart definition.
  // For Bar Chart and StackedBarChart, the values are y1, y2, y3, ....
  public static AbstractChart createChart(Cursor cursor, Object renderer, String sType, 
      HashMap<String, String> mAttributes, float fParentHeight) {    
    if (null == cursor || null == renderer || null == sType || null == mAttributes)
      return null;
    if (!(renderer instanceof DefaultRenderer))
      return null;
    PieChartHelper chartHelper = new PieChartHelper(cursor, sType, mAttributes);
    chartHelper.renderer = (DefaultRenderer)renderer;

    if (false == chartHelper.readDataset())
      return null;
    return chartHelper.createChart();
  }

  // This function assumes that the cursor contains 3 columns. title, color, value. The rows are ordered by title.  
  private boolean readDataset() {
    int iCount = cursor.getCount(), iColumnCount = cursor.getColumnCount();
    if (iColumnCount != 3)
      return false;
    int iTitleIndex = cursor.getColumnIndex("title");
    int iColorIndex = cursor.getColumnIndex("color");
    int iValueIndex = cursor.getColumnIndex("value");
    
    sTitles = new String[iCount];
    values = new double[iCount];
    iColors = new int[iCount];
    int iRow = 0; 
    cursor.moveToFirst();
    do {
      sTitles[iRow] = cursor.getString(iTitleIndex);
      values[iRow] = cursor.getDouble(iValueIndex);
      iColors[iRow] = Color.parseColor(cursor.getString(iColorIndex));      
      iRow++;
    } while (cursor.moveToNext());
    
    if (null == values || values.length == 0)
      return false;
    
    return true;
  }

  protected AbstractChart createChart() {
    for (int color : iColors) {
      SimpleSeriesRenderer r = new SimpleSeriesRenderer();
      r.setColor(color);
      renderer.addSeriesRenderer(r);
    }

    CategorySeries series = new CategorySeries(sType);
    int k = 0;
    for (double value : values) {
      series.add(sTitles[k++], value);
    }
    if (sType.equalsIgnoreCase("PieChart"))
      return new PieChart(series, renderer);
    else if (sType.equalsIgnoreCase("DialChart"))
      return new DialChart(series, (DialRenderer)renderer);
    return null;
  } 

}
