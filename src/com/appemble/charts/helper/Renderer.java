/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.helper;

import java.util.HashMap;

import org.achartengine.chart.PointStyle;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.DialRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation;
import org.achartengine.renderer.XYSeriesRenderer;

import com.appemble.avm.Utilities;

import android.graphics.Color;
import android.graphics.Paint.Align;

public class Renderer {
  
  public static Object initializeDefaultRenderer(Object rend, HashMap<String, String> mAttributes,
      float fParentHeight) {
    DefaultRenderer renderer = null;
    if (null == rend)
      renderer = new DefaultRenderer();
    else
      renderer = (DefaultRenderer) rend;
    String s = mAttributes.get("chart_title");
    if (null != s && s.length() > 0)
      renderer.setChartTitle(s.trim());
    s = mAttributes.get("chart_title_text_size");
    if (null != s && s.length() > 0)
      renderer.setChartTitleTextSize(Utilities.getDimension(s, fParentHeight));
    s = mAttributes.get("background_color");
    if (null != s && s.length() > 0) {
      renderer.setApplyBackgroundColor(true);
      renderer.setBackgroundColor(Color.parseColor(s.trim()));
    }
    s = mAttributes.get("show_axes");
    if (null != s && s.length() > 0)
      renderer.setShowAxes(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("axes_color");
    if (null != s && s.length() > 0)
      renderer.setAxesColor(Color.parseColor(s.trim()));
    s = mAttributes.get("show_labels");
    if (null != s && s.length() > 0)
      renderer.setShowLabels(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("labels_color");
    if (null != s && s.length() > 0)
      renderer.setLabelsColor(Color.parseColor(s.trim()));
    s = mAttributes.get("labels_text_size");
    if (null != s && s.length() > 0)
      renderer.setLabelsTextSize(Utilities.getDimension(s, fParentHeight));
    s = mAttributes.get("show_legend");
    if (null != s && s.length() > 0)
      renderer.setShowLegend(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("legend_text_size");
    if (null != s && s.length() > 0)
      renderer.setLegendTextSize(Utilities.getDimension(s, fParentHeight));
    s = mAttributes.get("legent_fit");
    if (null != s && s.length() > 0)
      renderer.setFitLegend(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("show_grid");
    if (null != s && s.length() > 0) {
      renderer.setShowGridX(Utilities.parseBoolean(s.trim()));
      renderer.setShowGridY(Utilities.parseBoolean(s.trim()));
    }
    s = mAttributes.get("show_x_grid");
    if (null != s && s.length() > 0)
      renderer.setShowGridX(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("show_y_Grid");
    if (null != s && s.length() > 0)
      renderer.setShowGridY(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("antialising");
    if (null != s && s.length() > 0)
      renderer.setAntialiasing(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("legend_height");
    if (null != s && s.length() > 0)
      renderer.setLegendHeight(Integer.parseInt(s.trim()));
    s = mAttributes.get("margins");
    if (null != s && s.length() > 0) {
      String[] sMargins = s.split(",");
      try {
        int[] margins = { 
            (int)Utilities.getDimension(sMargins[0], fParentHeight), 
            (int)Utilities.getDimension(sMargins[1], fParentHeight),
            (int)Utilities.getDimension(sMargins[2], fParentHeight),
            (int)Utilities.getDimension(sMargins[3], fParentHeight)
        };
        renderer.setMargins(margins);
      } catch (NumberFormatException nfe) {
        nfe.printStackTrace();
      }
    }
    s = mAttributes.get("chart_scale");
    if (null != s && s.length() > 0)
      renderer.setScale(Float.parseFloat(s.trim()));
    s = mAttributes.get("pan_enabled");
    if (null != s && s.length() > 0)
      renderer.setPanEnabled(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("zoom_enabled");
    if (null != s && s.length() > 0)
      renderer.setZoomEnabled(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("zoom_button_visible");
    if (null != s && s.length() > 0)
      renderer.setZoomButtonsVisible(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("zoom_rate");
    if (null != s && s.length() > 0)
      renderer.setZoomRate(Float.parseFloat(s.trim()));
    s = mAttributes.get("external_zoom_enabled");
    if (null != s && s.length() > 0)
      renderer.setExternalZoomEnabled(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("click_enabled");
    if (null != s && s.length() > 0)
      renderer.setClickEnabled(Utilities.parseBoolean(s.trim()));
//    s = mAttributes.get("clickablePoint");
//    if (null != s && s.length() > 0)
//      renderer.set(Integer.parseInt(s.trim()));
    s = mAttributes.get("display_values");
    if (null != s && s.length() > 0)
      renderer.setDisplayValues(Utilities.parseBoolean(s.trim()));

    s = mAttributes.get("in_scroll");
    if (null != s && s.length() > 0)
      renderer.setInScroll(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("start_angle");
    if (null != s && s.length() > 0)
      renderer.setStartAngle(Float.parseFloat(s.trim()));
    
    return renderer;
  }
  
  public static Object initializeDialRenderer(HashMap<String, String> mAttributes, float fParentHeight) {
    DialRenderer renderer = new DialRenderer();
    renderer = (DialRenderer) initializeDefaultRenderer(renderer, mAttributes, fParentHeight);
    
    String s = mAttributes.get("min_angle");
    if (null != s && s.length() > 0)
      renderer.setAngleMin(Double.parseDouble(s.trim()));
    s = mAttributes.get("max_angle");
    if (null != s && s.length() > 0)
      renderer.setAngleMax(Double.parseDouble(s.trim()));
    s = mAttributes.get("min_value");
    if (null != s && s.length() > 0)
      renderer.setMinValue(Double.parseDouble(s.trim()));
    s = mAttributes.get("max_value");
    if (null != s && s.length() > 0)
      renderer.setMaxValue(Double.parseDouble(s.trim()));
    s = mAttributes.get("minor_ticks_spacing");
    if (null != s && s.length() > 0)
      renderer.setMinorTicksSpacing(Double.parseDouble(s.trim()));
    s = mAttributes.get("major_ticks_spacing");
    if (null != s && s.length() > 0)
      renderer.setMajorTicksSpacing(Double.parseDouble(s.trim()));
    
    s = mAttributes.get("visual_types");
    if (null != s && s.length() > 0) {
      String[] sTypes = Utilities.splitCommaSeparatedString(s);
      DialRenderer.Type[] lTypes = new DialRenderer.Type[sTypes.length];
      int i = 0;
      for (String sType : sTypes) {
        sType = sType.trim();
        if (sType.equalsIgnoreCase("NEEDLE"))
          lTypes[i] = DialRenderer.Type.NEEDLE;
        else if (sType.equalsIgnoreCase("ARROW"))
          lTypes[i] = DialRenderer.Type.ARROW;
        i++;
      }
      renderer.setVisualTypes(lTypes);
    }
    return renderer;
  }

  public static Object initializeXYMultipleSeriesRenderer(HashMap<String, String> mAttributes,
      float fParentHeight) {
    XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
    renderer = (XYMultipleSeriesRenderer) initializeDefaultRenderer(renderer, mAttributes, fParentHeight);
    
    String s = mAttributes.get("x_axis_title");
    if (null != s && s.length() > 0)
      renderer.setXTitle(s.trim());
    s = mAttributes.get("y_axis_title");
    if (null != s && s.length() > 0)
      renderer.setYTitle(s.trim());
    s = mAttributes.get("axes_title_text_size");
    if (null != s && s.length() > 0)
      renderer.setAxisTitleTextSize(Utilities.getDimension(s, fParentHeight));
    
    s = mAttributes.get("x_min");
    try {
      if (null != s && s.length() > 0) 
        renderer.setXAxisMin(Double.parseDouble(s.trim()));
    } catch (NumberFormatException nfe) {}
    s = mAttributes.get("x_max");
    try {
    if (null != s && s.length() > 0)
      renderer.setXAxisMax(Double.parseDouble(s.trim()));
    } catch (NumberFormatException nfe) {}
    s = mAttributes.get("y_min");
    try {
    if (null != s && s.length() > 0)
      renderer.setYAxisMin(Double.parseDouble(s.trim()));
    } catch (NumberFormatException nfe) {}
    s = mAttributes.get("y_max");
    try {
    if (null != s && s.length() > 0)
      renderer.setYAxisMax(Double.parseDouble(s.trim()));
    } catch (NumberFormatException nfe) {}
    
    s = mAttributes.get("num_x_labels");
    if (null != s && s.length() > 0)
      renderer.setXLabels(Integer.parseInt(s.trim()));
    s = mAttributes.get("num_y_labels");
    if (null != s && s.length() > 0)
      renderer.setYLabels(Integer.parseInt(s.trim()));
    for (int i = 0; i < 20; i++) { // max 20 allowed.
      s = mAttributes.get("x_text_label" + i);
      if (null != s && s.length() > 0)
        renderer.addXTextLabel(i, s.trim());
    }
    s = mAttributes.get("num_y_labels");
    if (null != s && s.length() > 0)
      renderer.setYLabels(Integer.parseInt(s.trim()));
    for (int i = 0; i < 20; i++) {
      s = mAttributes.get("y_text_label" + i);
      if (null != s && s.length() > 0)
        renderer.addYTextLabel(i, s.trim());
    }
    s = mAttributes.get("orientation");
    if (s != null && s.length() > 0)
        renderer.setOrientation(s.equalsIgnoreCase("vertical") ? Orientation.VERTICAL : Orientation.HORIZONTAL);

    s = mAttributes.get("pan_enabled");
    if (null != s && s.length() > 0)
      renderer.setPanEnabled(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("zoom_enabled");
    if (null != s && s.length() > 0)
      renderer.setZoomEnabled(Utilities.parseBoolean(s.trim()));
    s = mAttributes.get("bar_spacing");
    if (null != s && s.length() > 0)
      renderer.setBarSpacing(Utilities.getDimension(s.trim(), fParentHeight));
    s = mAttributes.get("margins_color");
    if (null != s && s.length() > 0)
      renderer.setMarginsColor(Color.parseColor(s.trim()));
    
    s = mAttributes.get("pan_limits");
    if (null != s && s.length() > 0) {
      String[] sPanLimits = s.split(",");
      double[] panLimits = { 
          (double)Utilities.getDimension(sPanLimits[0], fParentHeight), 
          (double)Utilities.getDimension(sPanLimits[1], fParentHeight),
          (double)Utilities.getDimension(sPanLimits[2], fParentHeight),
          (double)Utilities.getDimension(sPanLimits[3], fParentHeight)
      };
      renderer.setPanLimits(panLimits);
    }
    s = mAttributes.get("zoom_limits");
    if (null != s && s.length() > 0) {
      String[] sZoomLimits = s.split(",");
      double[] zoomLimits = { 
          (double)Utilities.getDimension(sZoomLimits[0], fParentHeight), 
          (double)Utilities.getDimension(sZoomLimits[1], fParentHeight),
          (double)Utilities.getDimension(sZoomLimits[2], fParentHeight),
          (double)Utilities.getDimension(sZoomLimits[3], fParentHeight)
      };
      renderer.setZoomLimits(zoomLimits);
    }
    s = mAttributes.get("x_label_angle");
    if (null != s && s.length() > 0)
      renderer.setXLabelsAngle(Float.parseFloat(s.trim()));
    s = mAttributes.get("y_label_angle");
    if (null != s && s.length() > 0)
      renderer.setYLabelsAngle(Float.parseFloat(s.trim()));
    
    s = mAttributes.get("initial_range");
    if (null != s && s.length() > 0) {
      String[] sRanges = s.split(",");
      double[] ranges = { 
          (double)Utilities.getDimension(sRanges[0], fParentHeight), 
          (double)Utilities.getDimension(sRanges[1], fParentHeight),
          (double)Utilities.getDimension(sRanges[2], fParentHeight),
          (double)Utilities.getDimension(sRanges[3], fParentHeight)
      };
      renderer.setInitialRange(ranges);
    }
    s = mAttributes.get("point_size");
    if (null != s && s.length() > 0)
      renderer.setPointSize(Utilities.getDimension(s, fParentHeight));
    s = mAttributes.get("grid_color");
    if (null != s && s.length() > 0)
      renderer.setGridColor(Color.parseColor(s.trim()));

    s = mAttributes.get("x_label_align");
    if (s != null && s.length() > 0) {
      if (s.equalsIgnoreCase("LEFT"))
        renderer.setXLabelsAlign(Align.LEFT);
      else if (s.equalsIgnoreCase("CENTER"))
        renderer.setXLabelsAlign(Align.CENTER);
      else if (s.equalsIgnoreCase("RIGHT"))
        renderer.setXLabelsAlign(Align.RIGHT);
    }
    s = mAttributes.get("y_label_align");
    if (s != null && s.length() > 0) {
      if (s.equalsIgnoreCase("LEFT"))
        renderer.setYLabelsAlign(Align.LEFT);
      else if (s.equalsIgnoreCase("CENTER"))
        renderer.setYLabelsAlign(Align.CENTER);
      else if (s.equalsIgnoreCase("RIGHT"))
        renderer.setYLabelsAlign(Align.RIGHT);
    }
    s = mAttributes.get("y_axis_align");
    if (s != null && s.length() > 0) {
      if (s.equalsIgnoreCase("LEFT"))
        renderer.setYAxisAlign(Align.LEFT, 0);
      else if (s.equalsIgnoreCase("CENTER"))
        renderer.setYAxisAlign(Align.LEFT, 0);
      else if (s.equalsIgnoreCase("RIGHT"))
        renderer.setYAxisAlign(Align.RIGHT, 0);
    }
    s = mAttributes.get("x_label_color");
    if (null != s && s.length() > 0)
      renderer.setXLabelsColor(Color.parseColor(s.trim()));
    s = mAttributes.get("y_label_color");
    if (null != s && s.length() > 0)
      renderer.setYLabelsColor(0, Color.parseColor(s.trim()));
    
    s = mAttributes.get("x_label_padding");
    if (null != s && s.length() > 0)
      renderer.setXLabelsPadding(Utilities.getDimension(s, fParentHeight));
    s = mAttributes.get("y_label_padding");
    if (null != s && s.length() > 0)
      renderer.setYLabelsPadding(Utilities.getDimension(s, fParentHeight));
    return renderer;
  }

  public static Object initializeSimpleSeriesRenderer(Object rend, HashMap<String, String> mAttributes, 
      float fParentHeight) {
    SimpleSeriesRenderer renderer = null;
    if (null == rend)
      renderer = new SimpleSeriesRenderer();
    else
      renderer = (SimpleSeriesRenderer) rend;
    String s = mAttributes.get("color");
    if (null != s && s.length() > 0)
      renderer.setColor(Color.parseColor(s.trim()));
/* commented with V1.1 of chart engine    
    s = mAttributes.get("display_chart_values");
    if (null != s && s.length() > 0)
      renderer.setDisplayChartValues(Boolean.parseBoolean(s.trim()));
    s = mAttributes.get("display_chart_values_spacing");
    if (null != s && s.length() > 0)
      renderer.setDisplayChartValuesDistance((int)Utilities.getDimension(s, fParentHeight));
    s = mAttributes.get("chart_values_text_size");
    if (null != s && s.length() > 0)
      renderer.setChartValuesTextSize((int)Utilities.getDimension(s, fParentHeight));
    s = mAttributes.get("chart_values_text_align");
    if (s != null && s.length() > 0) {
      if (s.equalsIgnoreCase("LEFT"))
        renderer.setChartValuesTextAlign(Align.LEFT);
      else if (s.equalsIgnoreCase("CENTER"))
        renderer.setChartValuesTextAlign(Align.CENTER);
      else if (s.equalsIgnoreCase("RIGHT"))
        renderer.setChartValuesTextAlign(Align.RIGHT);
    }

    s = mAttributes.get("chart_values_spacing");
    if (null != s && s.length() > 0)
      renderer.setChartValuesSpacing((int)Utilities.getDimension(s, fParentHeight));
*/    
    s = mAttributes.get("stroke");
    if (s != null && s.length() > 0) {
      if (s.equalsIgnoreCase("SOLID"))
        renderer.setStroke(BasicStroke.SOLID);
      else if (s.equalsIgnoreCase("DASHED"))
        renderer.setStroke(BasicStroke.DASHED);
      else if (s.equalsIgnoreCase("DOTTED"))
        renderer.setStroke(BasicStroke.DOTTED);
    }
    s = mAttributes.get("gradient_enabled");
    if (null != s && s.length() > 0)
      renderer.setGradientEnabled(Boolean.parseBoolean(s.trim()));
    Double dStart = 0.0; int color = 0;
    s = mAttributes.get("gradient_start_value");
    if (null != s && s.length() > 0)
      dStart = Double.parseDouble(s.trim());
    s = mAttributes.get("gradient_start_color");
    if (null != s && s.length() > 0)
      color = Color.parseColor(s.trim());
    if (dStart != 0.0 && color != 0)
      renderer.setGradientStart(dStart, color);
    dStart = 0.0; color = 0;
    s = mAttributes.get("gradient_stop_value");
    if (null != s && s.length() > 0)
      dStart = Double.parseDouble(s.trim());
    s = mAttributes.get("gradient_stop_color");
    if (null != s && s.length() > 0)
      color = Color.parseColor(s.trim());
    if (dStart != 0.0 && color != 0)
      renderer.setGradientStop(dStart, color);
    return renderer;
  }  

  public static Object initializeXYSeriesRenderer(HashMap<String, String> mAttributes, float fParentHeight) {
    XYSeriesRenderer renderer = new XYSeriesRenderer();
    renderer = (XYSeriesRenderer) initializeSimpleSeriesRenderer(renderer, mAttributes, fParentHeight);
    String s = mAttributes.get("fill_points");
    if (null != s && s.length() > 0)
      renderer.setFillPoints(Boolean.parseBoolean(s.trim()));
    s = mAttributes.get("point_stroke_width");
    if (null != s && s.length() > 0)
      renderer.setPointStrokeWidth((int)Utilities.getDimension(s.trim(), fParentHeight));
    s = mAttributes.get("fill_below_line");
    if (null != s && s.length() > 0)
      renderer.setFillBelowLine(Boolean.parseBoolean(s.trim()));
    s = mAttributes.get("fill_color");
    if (null != s && s.length() > 0)
      renderer.setFillBelowLineColor(Color.parseColor(s.trim()));

    s = mAttributes.get("point_style");
    if (null != s && s.length() > 0) {
        s = s.trim(); 
        if (s.equalsIgnoreCase("CIRCLE"))
          renderer.setPointStyle(PointStyle.CIRCLE);
        else if (s.equalsIgnoreCase("TRIANGLE"))
          renderer.setPointStyle(PointStyle.TRIANGLE);
        else if (s.equalsIgnoreCase("DIAMOND"))
          renderer.setPointStyle(PointStyle.DIAMOND);
        else if (s.equalsIgnoreCase("SQUARE"))
          renderer.setPointStyle(PointStyle.SQUARE);
        else if (s.equalsIgnoreCase("POINT"))
          renderer.setPointStyle(PointStyle.POINT);
        else if (s.equalsIgnoreCase("X"))
          renderer.setPointStyle(PointStyle.X);
        else
          System.out.println("Point Style must be CIRCLE, TRIANGLE, DIAMOND, SQUARE, POINT or X");
      }

    s = mAttributes.get("line_width");
    if (null != s && s.length() > 0)
      renderer.setLineWidth((int)Utilities.getDimension(s.trim(), fParentHeight));
    return renderer;
  }

  /*
  boolean initialize() {
    if (null != mRenderer || bOnMeasure)
      return true;

    if (null == controlModel || null == controlModel.sMisc1 || controlModel.sMisc1.length() == 0)
      return false;

    mRenderer = new XYMultipleSeriesRenderer();

    try {
      XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
      factory.setNamespaceAware(true);
      XmlPullParser xpp = factory.newPullParser();
      xpp.setInput(new StringReader(controlModel.sMisc1));

      while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
        if (xpp.getEventType() == XmlPullParser.START_TAG) {
          String tagName = xpp.getName();
          if (tagName.equals("chart")) {
            sType = xpp.getAttributeValue(null, "type");
            String s = xpp.getAttributeValue(null, "chartTitle");
            if (null != s && s.length() > 0)
              mRenderer.setChartTitle(s.trim());
            s = xpp.getAttributeValue(null, "chartTitleTextSize");
            if (null != s && s.length() > 0)
              mRenderer.setChartTitleTextSize(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "background");
            mRenderer.setApplyBackgroundColor(null != s && s.length() > 0);
            if (null != s && s.length() > 0)
              mRenderer.setBackgroundColor(Color.parseColor(s.trim()));
            s = xpp.getAttributeValue(null, "pointSize");
            if (null != s && s.length() > 0)
              mRenderer.setPointSize(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "orientation");
            if (s != null && s.length() > 0) {
              if (s.equalsIgnoreCase("vertical"))
                mRenderer.setOrientation(Orientation.VERTICAL);
              else
                mRenderer.setOrientation(Orientation.HORIZONTAL);
            }

            s = xpp.getAttributeValue(null, "xMin");
            if (s != null && s.length() > 0)
              mRenderer.setXAxisMin(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "xMax");
            if (s != null && s.length() > 0)
              mRenderer.setXAxisMax(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "yMin");
            if (s != null && s.length() > 0)
              mRenderer.setYAxisMin(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "yMax");
            if (s != null && s.length() > 0)
              mRenderer.setYAxisMax(Float.parseFloat(s.trim()));

            s = xpp.getAttributeValue(null, "showAxis");
            if (s != null && s.length() > 0)
              mRenderer.setShowAxes(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "xAxisTitle");
            if (s != null && s.length() > 0)
              mRenderer.setXTitle(s.trim());
            s = xpp.getAttributeValue(null, "yAxisTitle");
            if (s != null && s.length() > 0)
              mRenderer.setYTitle(s.trim());
            s = xpp.getAttributeValue(null, "axisColor");
            if (null != s && s.length() > 0)
              mRenderer.setAxesColor(Color.parseColor(s.trim()));
            s = xpp.getAttributeValue(null, "axisTitleTextSize");
            if (s != null && s.length() > 0)
              mRenderer.setAxisTitleTextSize(Float.parseFloat(s.trim()));

            s = xpp.getAttributeValue(null, "showLabels");
            if (s != null && s.length() > 0)
              mRenderer.setShowLabels(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "labelColor");
            if (null != s && s.length() > 0)
              mRenderer.setLabelsColor(Color.parseColor(s.trim()));
//            
//              s = xpp.getAttributeValue(null, "yLabelColor"); if (null != s &&
//              s.length() > 0) mRenderer.setLYLabelsColor(1,
//              Color.parseColor(s.trim()));
//             
            s = xpp.getAttributeValue(null, "labelsTextSize");
            if (s != null && s.length() > 0)
              mRenderer.setLabelsTextSize(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "xLabelAlign");
            if (s != null && s.length() > 0) {
              if (s.equalsIgnoreCase("LEFT"))
                mRenderer.setXLabelsAlign(Align.LEFT);
              else if (s.equalsIgnoreCase("CENTER"))
                mRenderer.setXLabelsAlign(Align.CENTER);
              else if (s.equalsIgnoreCase("RIGHT"))
                mRenderer.setXLabelsAlign(Align.RIGHT);
            }
            s = xpp.getAttributeValue(null, "yLabelAlign");
            if (s != null && s.length() > 0) {
              if (s.equalsIgnoreCase("LEFT"))
                mRenderer.setYLabelsAlign(Align.LEFT);
              else if (s.equalsIgnoreCase("CENTER"))
                mRenderer.setYLabelsAlign(Align.CENTER);
              else if (s.equalsIgnoreCase("RIGHT"))
                mRenderer.setYLabelsAlign(Align.RIGHT);
            }
            s = xpp.getAttributeValue(null, "xLabelAngle");
            if (s != null && s.length() > 0)
              mRenderer.setXLabelsAngle(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "yLabelAngle");
            if (s != null && s.length() > 0)
              mRenderer.setYLabelsAngle(Float.parseFloat(s.trim()));

            s = xpp.getAttributeValue(null, "showLegend");
            if (null != s && s.length() > 0)
              mRenderer.setShowLegend(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "legendTextSize");
            if (null != s && s.length() > 0)
              mRenderer.setLabelsTextSize(Float.parseFloat(s.trim()));
            s = xpp.getAttributeValue(null, "legendFit");
            if (null != s && s.length() > 0)
              mRenderer.setFitLegend(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "legendHeight");
            if (null != s && s.length() > 0)
              mRenderer.setLegendHeight(Integer.parseInt(s.trim()));

            s = xpp.getAttributeValue(null, "margins");
            if (null != s && s.length() > 0) {
              String[] sMargins = s.split(",");
              try {
                int[] margins = { Integer.parseInt(sMargins[0]), Integer.parseInt(sMargins[1]),
                    Integer.parseInt(sMargins[2]), Integer.parseInt(sMargins[3]) };
                mRenderer.setMargins(margins);
              } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
              }
            }

            s = xpp.getAttributeValue(null, "marginsColor");
            if (null != s && s.length() > 0)
              mRenderer.setMarginsColor(Color.parseColor(s.trim()));
            s = xpp.getAttributeValue(null, "panEnabled");
            if (null != s && s.length() > 0)
              mRenderer.setPanEnabled(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "panLimits");
            if (null != s && s.length() > 0) {
              String[] sPanLimits = s.split(",");
              double[] panLimits = { Double.parseDouble(sPanLimits[0]),
                  Double.parseDouble(sPanLimits[1]), Double.parseDouble(sPanLimits[2]),
                  Double.parseDouble(sPanLimits[3]) };
              mRenderer.setPanLimits(panLimits);
            }
            s = xpp.getAttributeValue(null, "zoomEnabled");
            if (null != s && s.length() > 0)
              mRenderer.setZoomEnabled(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "zoomLimits");
            if (null != s && s.length() > 0) {
              String[] sZoomLimits = s.split(",");
              double[] zoomLimits = { Double.parseDouble(sZoomLimits[0]),
                  Double.parseDouble(sZoomLimits[1]), Double.parseDouble(sZoomLimits[2]),
                  Double.parseDouble(sZoomLimits[3]) };
              mRenderer.setZoomLimits(zoomLimits);
            }
            s = xpp.getAttributeValue(null, "zoomRate");
            if (s != null && s.length() > 0)
              mRenderer.setZoomRate(Float.parseFloat(s.trim()));

            s = xpp.getAttributeValue(null, "barSpacing");
            if (null != s && s.length() > 0)
              mRenderer.setBarSpacing(Float.parseFloat(s.trim()));

            s = xpp.getAttributeValue(null, "showGrid");
            if (null != s && s.length() > 0)
              mRenderer.setShowGrid(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "showXGrid");
            if (null != s && s.length() > 0)
              mRenderer.setShowGridX(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "showYGrid");
            if (null != s && s.length() > 0)
              mRenderer.setShowGridY(Boolean.parseBoolean(s.trim()));
            s = xpp.getAttributeValue(null, "gridColor");
            if (null != s && s.length() > 0)
              mRenderer.setGridColor(Color.parseColor(s.trim()));

            s = xpp.getAttributeValue(null, "antialiasing");
            if (null != s && s.length() > 0)
              mRenderer.setAntialiasing(Boolean.parseBoolean(s.trim()));

            // get chart specific data
            s = xpp.getAttributeValue(null, "colors");
            String[] sColors = null;
            if (null != s && s.length() > 0)
              sColors = s.split(",");
            s = xpp.getAttributeValue(null, "pointStyles");
            String[] sPointStyles = null;
            PointStyle[] pointStyles = null;
            if (null != s && s.length() > 0) {
              sPointStyles = s.split(",");
              pointStyles = new PointStyle[sPointStyles.length];
              for (int i = 0; i < sPointStyles.length; i++) {
                String sPointStyle = sPointStyles[i].trim(); 
                if (sPointStyle.equalsIgnoreCase("CIRCLE"))
                  pointStyles[i] = PointStyle.CIRCLE;
                else if (sPointStyle.equalsIgnoreCase("TRIANGLE"))
                  pointStyles[i] = PointStyle.TRIANGLE;
                else if (sPointStyle.equalsIgnoreCase("DIAMOND"))
                  pointStyles[i] = PointStyle.DIAMOND;
                else if (sPointStyle.equalsIgnoreCase("SQUARE"))
                  pointStyles[i] = PointStyle.SQUARE;
                else if (sPointStyle.equalsIgnoreCase("POINT"))
                  pointStyles[i] = PointStyle.POINT;
                else
                  System.out.println("Point Style must be CIRCLE, TRIANGLE, DIAMOND, SQUARE or POINT");
              }
            }
            if (sType.equalsIgnoreCase("BarChart") || sType.equalsIgnoreCase("StackedBarChart") || 
                sType.equalsIgnoreCase("DoughnutChart")) {
              try {
                for (int i = 0; i < sColors.length; i++) {
                  SimpleSeriesRenderer r = new SimpleSeriesRenderer();
                  r.setColor(Color.parseColor(sColors[i].trim()));
                  mRenderer.addSeriesRenderer(r);
                }
              } catch (IllegalArgumentException iae) {
                iae.printStackTrace();
              }
            } else if (sType.equalsIgnoreCase("LineChart") || sType.equalsIgnoreCase("CubicLineChart")) {
                try {
                  for (int i = 0; i < sColors.length; i++) {
                    XYSeriesRenderer r = new XYSeriesRenderer();
                    r.setColor(Color.parseColor(sColors[i].trim()));
                    r.setPointStyle(pointStyles[i]);
                    mRenderer.addSeriesRenderer(r);
                  }
                } catch (IllegalArgumentException iae) {
                  iae.printStackTrace();
                }
            } else if (sType.equalsIgnoreCase("ABCChart")) {
              try {
                for (int i = 0; i < sColors.length; i++) {
//                  DefaultRenderer renderer = new DefaultRenderer();
                  XYSeriesRenderer r = new XYSeriesRenderer();
                  r.setColor(Color.parseColor(sColors[i].trim()));
                  r.setPointStyle(pointStyles[i]);
                  mRenderer.addSeriesRenderer(r);
                }
              } catch (IllegalArgumentException iae) {
                iae.printStackTrace();
              }
            }
            if (sType.equalsIgnoreCase("CubicLineChart")) {
              int length = mRenderer.getSeriesRendererCount();
              for (int i = 0; i < length; i++) {
                ((XYSeriesRenderer) mRenderer.getSeriesRendererAt(i)).setFillPoints(true);
              }
            }
//            
//             s = xpp.getAttributeValue(null, "numBarStacks"); if (null != s &&
//             s.length() > 0) { aTitles =
//             Utilities.splitCommaSeparatedString(s.trim()); }
//             
            // if (sType.contains("BarChart") || sType.contains("LineChart")) {
            s = xpp.getAttributeValue(null, "barTitles");
            if (null != s && s.length() > 0) {
              aTitles = Utilities.splitCommaSeparatedString(s.trim());
            }
            // }
          }
        }
        xpp.next();
      }
    } catch (XmlPullParserException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Throwable t) {
      t.printStackTrace();
      // Log error here
    }
    int length = mRenderer.getSeriesRendererCount();
    for (int i = 0; i < length; i++) {
      SimpleSeriesRenderer seriesRenderer = mRenderer.getSeriesRendererAt(i);
      if (seriesRenderer != null)
        seriesRenderer.setDisplayChartValues(true);
    }
//    
//      XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
//      
//      // setChartSettings
//      renderer.setChartTitle("Monthly sales in the last 2 years");
//      renderer.setXTitle("Month"); renderer.setYTitle("Units sold");
//      renderer.setXAxisMin(0.5); renderer.setXAxisMax(12.5);
//      renderer.setYAxisMin(0); renderer.setYAxisMax(24000);
//      renderer.setAxesColor(Color.GRAY); renderer.setLabelsColor(Color.LTGRAY);
//      
//      // buildBarRenderer renderer.setAxisTitleTextSize(16);
//      renderer.setChartTitleTextSize(20); renderer.setLabelsTextSize(15);
//      renderer.setLegendTextSize(15); int[] colors = new int[] { Color.CYAN,
//      Color.BLUE }; int length = colors.length; for (int i = 0; i < length;
//      i++) { SimpleSeriesRenderer r = new SimpleSeriesRenderer();
//      r.setColor(colors[i]); renderer.addSeriesRenderer(r); }
//      
//      renderer.getSeriesRendererAt(0).setDisplayChartValues(true);
//      renderer.getSeriesRendererAt(1).setDisplayChartValues(true);
//      renderer.setXLabels(12); renderer.setYLabels(10);
//      renderer.setXLabelsAlign(Align.LEFT);
//      renderer.setYLabelsAlign(Align.LEFT); renderer.setPanEnabled(true,
//      false); // renderer.setZoomEnabled(false); renderer.setZoomRate(1.1f);
//      renderer.setBarSpacing(0.5f); } catch (XmlPullParserException e) { //
//      TODO Auto-generated catch block e.printStackTrace(); } catch (Throwable
//      t) { // Log error here }
//     
    return true;
  }
*/
}
