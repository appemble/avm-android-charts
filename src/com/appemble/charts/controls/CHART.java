/*
 * Copyright (C) 2012 The Chart Plugin
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.charts.controls;

import java.util.Vector;

import org.achartengine.GraphicalView;
import org.achartengine.chart.AbstractChart;

import android.content.Context;
import android.database.Cursor;
import android.graphics.PointF;
import android.view.View;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.actions.Action;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.charts.helper.BarChartHelper;
import com.appemble.charts.helper.BubbleChartHelper;
import com.appemble.charts.helper.DoughnutChartHelper;
import com.appemble.charts.helper.LineChartHelper;
import com.appemble.charts.helper.PieChartHelper;
import com.appemble.charts.helper.RangeBarChartHelper;
import com.appemble.charts.helper.Renderer;
import com.appemble.charts.helper.TimeChartHelper;

public class CHART extends GraphicalView implements ControlInterface {
  ControlModel controlModel;
  String sImageSource;
  Object mRenderer = null;
  AbstractChart mChart = null;
  String sType = null;
  float fWidth, fHeight; // Using which the child control dimensions are to be calculated.

  // boolean bOnMeasure = false;

  public CHART(final Context context, final ControlModel controlObject) {
    super(context);
    controlModel = controlObject;
  }

  public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
      Vector<ControlModel> vChildControls, ScreenModel screenModel) {
    if (null == controlModel)
      return Boolean.valueOf(false);
    setFocusable(false);
    if (Cache.bDimenRelativeToParent) {
      PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
      fWidth = size.x; fHeight = size.y;
    } else {
      fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
    }
    
    if (controlModel.bActionYN) {
      setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
          Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
//          ActionModel[] actions = controlModel.getActions();
//          if (null != actions)
//            for (int i = 0; i < actions.length; i++) {
//              if (actions[i].iPrecedingActionId == 0) {
//                Action a = new Action();
//                a.execute(getContext(), v, parentView, actions[i]);
//              }
//            }
        }
      });
    }
    return Boolean.valueOf(true);
  }

  // @Override
  // public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
  // bOnMeasure = true;
  // super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  // bOnMeasure = false;
  // return;
  // }

  public int getControlId() {
    return controlModel.id;
  }

  public ControlModel getControlModel() {
    return controlModel;
  }

  public String getValue() {
    return null;
  }

  public void setValue(Object object) {
    // if (bOnMeasure)
    // return;
    if (!(object instanceof Cursor))
      return;
    Cursor cursor = (Cursor) object;
    if (0 == cursor.getCount()) {
        cursor.close();
        return; // no values to be set.
    }
    initialize();
    try {
      AbstractChart chart = null;
      if (sType.equalsIgnoreCase("CubicLineChart") || sType.equalsIgnoreCase("LineChart")
          || sType.equalsIgnoreCase("ScatterChart"))
        chart = LineChartHelper.createChart(cursor, mRenderer, sType,
            controlModel.mExtendedProperties, fHeight);
      else if (sType.equalsIgnoreCase("BarChart") || sType.equalsIgnoreCase("StackedBarChart"))
        chart = BarChartHelper.createChart(cursor, mRenderer, sType,
            controlModel.mExtendedProperties, fHeight);
      // sType.equalsIgnoreCase("CombinedXYChart") ||
      // setChart(ChartHelper.createChartFromXYMultipleSeries(cursor, mRenderer,
      // sType, mAttributes));
      else if (sType.equalsIgnoreCase("RangeBarChart"))
        chart = RangeBarChartHelper.createChart(cursor, mRenderer, sType,
            controlModel.mExtendedProperties, fHeight);
      else if (sType.equalsIgnoreCase("TimeChart"))
        chart = TimeChartHelper.createChart(cursor, mRenderer, sType,
            controlModel.mExtendedProperties, fHeight);
      else if (sType.equalsIgnoreCase("BubbleChart"))
        chart = BubbleChartHelper.createChart(cursor, mRenderer, sType,
            controlModel.mExtendedProperties, fHeight);
      else if (sType.equalsIgnoreCase("PieChart") || sType.equalsIgnoreCase("DialChart"))
        chart = PieChartHelper.createChart(cursor, mRenderer, sType,
            controlModel.mExtendedProperties, fHeight);
      else if (sType.equalsIgnoreCase("DoughnutChart"))
        chart = DoughnutChartHelper.createChart(cursor, mRenderer, sType,
            controlModel.mExtendedProperties, fHeight);
      if (null != chart)
//        return;
//      else
        setChart(chart);
    } catch (NumberFormatException nfe) {
      nfe.printStackTrace();
    }
    if (false == cursor.isClosed())
        cursor.close();
  }

  public boolean initialize() {
    if (null == controlModel || null == controlModel.mExtendedProperties
        || controlModel.mExtendedProperties.size() == 0)
      return false;
    if (null != mRenderer)
      return true;
    try {
      // mAttributes = Utilities.getNameValuePairMap(controlModel.sMisc1);
      sType = controlModel.mExtendedProperties.get("chart_type");
      if (sType.equalsIgnoreCase("CubicLineChart") || sType.equalsIgnoreCase("LineChart")
          || sType.equalsIgnoreCase("CombinedXYChart") || sType.equalsIgnoreCase("BarChart")
          || sType.equalsIgnoreCase("StackedBarChart") || sType.equalsIgnoreCase("ScatterChart")
          || sType.equalsIgnoreCase("TimeChart") || sType.equalsIgnoreCase("RangeBarChart")
          || sType.equalsIgnoreCase("BubbleChart"))
        mRenderer = (Object) Renderer
            .initializeXYMultipleSeriesRenderer(controlModel.mExtendedProperties, fHeight);
      else if (sType.equalsIgnoreCase("DoughnutChart") || sType.equalsIgnoreCase("PieChart"))
        mRenderer = (Object) Renderer.initializeDefaultRenderer(null,
            controlModel.mExtendedProperties, fHeight);
      else if (sType.equalsIgnoreCase("DialChart"))
        mRenderer = (Object) Renderer.initializeDialRenderer(controlModel.mExtendedProperties, fHeight);
    } catch (NumberFormatException nfe) {
      nfe.printStackTrace();
    }
    return true;
  }
}
